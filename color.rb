module Jekyll
	module ColorFilters
		extend self
		def shade(input, degree=0.5)
			throw "shade tag expects a number in the range [0..1], representing how much darker it should be, with 1 being completely black." unless degree.between?(0..1)
			r, g, b = parse_color(input).map {|item| item*(1-degree)}
			"\##{r.to_s(16)}#{g.to_s(16)}#{b.to_s(16)}"
		end
		def tint(input, degree=0.5)
			throw "tint tag expects a number in the range [0..1], representing how much lighter it should be, with 1 being completely white." unless degree.between?(0..1)
			r, g, b = parse_color(input).map {|item| item+(255-item)*degree}
			"\##{r.to_s(16)}#{g.to_s(16)}#{b.to_s(16)}"
		end
		def complement(input)
			r, g, b = parse_color(input).map {|item| 255-item}
			"\##{r.to_s(16)}#{g.to_s(16)}#{b.to_s(16)}"
		end

		private
		def parse_color(input)
			begin
				case input[0]
				when "#"
					return input[1..2].to_i(16),input[3..4].to_i(16),input[5..6].to_i(16)
				when "r"
					return input.delete("rgba() ").split(",").map(&:to_i)
				else
					throw "caught below, "+input[0]
				end
			rescue
				throw "Color tags expect a color in either hexadecimal \#RRGGBB format, rgb[a](r, g, b[, a]) with floating or integer r, g & b. a is ignored. You passed in #{input}. "+$!.message
			end
		end

	end
end

Liquid::Template.register_filter(Jekyll::ColorFilters)
